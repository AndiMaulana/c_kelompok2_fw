<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kategori_posting extends Model
{
    protected $table = 'kategori_posting';
    protected $fillable = ['id','id_posting','id_kategori'];
}
