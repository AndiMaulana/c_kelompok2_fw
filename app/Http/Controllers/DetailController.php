<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\detail;
use App\kategori;
use App\kategori_posting;
use DB;
use Auth;

class DetailController extends Controller
{
   public function index(){
   		$posts = DB::select('SELECT * FROM detail_user,users where detail_user.id_user=users.id order by detail_user.likes desc');
   		//$posts = detail::orderBy('likes','desc')->paginate(10);
   		$kategori = kategori::all();
        return view('detail',compact('kategori'))->with('details', $posts);
   }
   public function kategori($id){
   	$kategori1= DB::select('SELECT * FROM kategori_posting,posting,kategori,users where kategori.id=kategori_posting.id_kategori AND posting.id=kategori_posting.id_posting AND users.id=posting.user_id AND id_kategori='.$id);
   	//$kategori1=kategori_posting::where('id_kategori',$id)->get();
   	$kategori = kategori::all();
   	return view('index',compact('kategori'))->with('kategoris',$kategori1);
   }


   public function likes($id){
   	//$detail= DB::select('SELECT * FROM detail_user where id='.$id);
   	//$likes = detail::where('id_user',$id)->value('likes');
   	//$newlikes=  $likes+1;
   	//$detail->likes=$newlikes;
   	//$detail->save();
   	DB::table('detail_user')->where('id_user',$id)->increment('likes',1);
   	return redirect('posts');
   }

   public function decres($id){
   	DB::table('detail_user')->where('id_user',$id)->decrement('likes',1);
   	return redirect('posts');
   }
   public function createDetail(){
        $id=Auth::user()->id;
        $data = array(
                'id_user' => $id
            );
            $tambah =detail::insert($data);
        if($tambah){
            echo "berhasil";
        }else{
            echo "gagal";
        }
        return redirect('home');
    }
}
