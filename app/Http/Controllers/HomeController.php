<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\kategori;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index(){
    $user_id = auth()->user()->id;
        $user = User::find($user_id);
        $kategori = kategori::all();
    return view('Posting.mypost',compact('kategori'))->with('posts', $user->posts);
   }
}
