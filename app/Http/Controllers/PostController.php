<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use App\kategori;
class PostController extends Controller
{
   public function tampil(){
   	$user_id = auth()->user()->id;
        $user = User::find($user_id);
     $kategori=kategori::all();
   	return view('Posting.mypost',compact('kategori'))->with('posts', $user->posts);
   }
}
