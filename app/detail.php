<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class detail extends Model
{
   protected $table = 'detail_user';
    protected $fillable = ['id','id_user'];
   
    public function User(){
    	return $this->belongsTo('App\User');
    }
}
