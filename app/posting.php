<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class posting extends Model
{
    protected $table = 'posting';
    protected $fillable = ['id','judul','gambar','keterangan','user_id'];
}
