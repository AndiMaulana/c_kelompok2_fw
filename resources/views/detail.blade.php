@extends('layouts.app3')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Top Likes</div>

                <div class="panel-body">
                    
                    @if(count($details) > 0)
                        <table class="table table-striped">
                            <tr>
                                <th>User</th>
                                <th>Likes</th>
                                <th>Posting</th>
                            </tr>
                            @foreach($details as $post)
                                <tr>
                                    <td>{{$post->name}}</td>
                                    <td>{{$post->likes}}</td>
                                    <td>{{$post->postingan}}</td>
                                </tr>
                            @endforeach
                        </table>
                    @else
                        <p>You have no posts</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
