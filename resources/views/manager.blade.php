@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Manage User</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
<div class="panel-body">
<table class="table table-striped">
    <tr>
        <th>Judul</th>
        <th>Aksi</th>
    </tr>
    @foreach ($user as $row)
    <tr>
        <td>{{$row->name}}</td>
        <td>
            <a href="{{url("user/hapus/".$row->id)}}"class="btn btn-danger btn-xs">Hapus</a>
        </td>

    </tr>
    @endforeach
</TABLE>
</div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
