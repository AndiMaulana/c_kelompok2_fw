@extends('layouts.app3')

@section('content')
    <a href="/posts" class="btn btn-default">Go Back</a>
     <div class="panel-body">
    <h1>{{$post->judul}}</h1>
    <img style="width:50%" src="/uploads/post/{{$post->gambar}}">
    <br><br>
    <div>
        {!!$post->keterangan!!}
    </div>
    <hr>
    <small>Written on {{$post->created_at}} by {{$post->user->name}}</small>
    <hr>
    @if(!Auth::guest())
        @if(Auth::user()->id == $post->user_id)

            <a href="/posts/{{$post->id}}/edit" class="btn btn-default">Edit</a>

            {!!Form::open(['action' => ['PostsController@destroy', $post->id], 'method' => 'POST', 'class' => 'pull-right'])!!}
                {{Form::hidden('_method', 'DELETE')}}
                {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}
            {!!Form::close()!!}
        @endif
    @endif
    </div>
@endsection