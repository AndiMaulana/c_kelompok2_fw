@extends('layouts.app3')

@section('content')
    <h1>Posts</h1>
    @if(count($posts) > 0)
        @foreach($posts as $post)
             <div class="panel-body">
            <div class="well">
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <img style="width:100%" src="/uploads/post/{{$post->gambar}}">
                    </div>
                    <div class="col-md-8 col-sm-8">
                        <h3><a href="/posts/{{$post->id}}">{{$post->judul}}</a></h3>
                        <small>Written on {{$post->created_at}} by {{$post->user->name}}</small>
                        <br><br><br><br>
                    <a href="/likes/{{$post->user_id}}" class="btn btn-primary">LIKE</a>
                    <a href="/decre/{{$post->user_id}}" class="btn btn-danger">UNLIKE</a>
                    
                    @if(Auth::user()->level == "admin")
                    <a href="/hapus/{{$post->user_id}}" class="btn btn-danger">Hapus</a>
                    @endif
                    
                    </div>

                </div>
            </div>
            
            </div>

        @endforeach
        {{$posts->links()}}
    @else
        <p>No posts found</p>
    @endif
@endsection