<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::post('/penulis/simpan',"penulisController@simpan");
Route::get('/penulis',"penulisController@awal");
Route::get('/penulis/edit/{penulis}',"penulisController@edit");
Route::post('/penulis/update/{penulis}',"penulisController@update");
Route::get('/penulis/hapus/{penulis}',"penulisController@hapus");*/



Route::get('/','AuthController@index');
Route::get('/login','AuthController@formLogin');
Route::post('/login','AuthController@proses');
Route::get('/logout','AuthController@logout');
Route::get('/', function () {
    return view('home');
});
/*Route::get('/upload','LaguController@index');
Route::post('/lagu','LaguController@upload');
Route::get('/play','LaguController@play');
Route::get('lagu/detail/{lagu}','LaguController@detail');
Route::get('lagu/ubah/{lagu}','LaguController@edit');
Route::post('/lagu/update/{lagu}','LaguController@update');
Route::get('/lagu/hapus/{lagu}','LaguController@hapus');
Route::get('/playlist','PlaylistController@index');
Route::get('/playlist/tambah','PlaylistController@tambah');
Route::get('/playlist/tampil/{playlist}','PlaylistController@tampil');
Route::get('/isi','isiPlaylistController@index');
Route::post('/playlist/simpan','PlaylistController@simpan');*/
Auth::routes();

Route::group(['middleware'=> ['web']], function()
{
	Route::group(['middleware' => ['Admin']], function(){

Route::get('profile', 'UserController@profile');
Route::post('profile', 'UserController@update_avatar');
Route::get('/tampil/post', 'PostController@tampil');
Route::get('/top/user', 'DetailController@index');
Route::get('/kategori/{kategori}','DetailController@kategori');
Route::get('/likes/{likes}','DetailController@likes');
Route::get('/decre/{likes}','DetailController@decres');
Route::resource('posts', 'PostsController');
Route::get('/about',function(){
	return view('about');
});
Route::get('user/manager',function(){
	$user=Auth::user()->all()->where('level','user');
	return view('manager',compact('user'));
});

Route::get('user/hapus/{user}','UserController@hapus');



Route::get('/home1','DetailController@createDetail')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
});

Route::group(['middleware' => ['User']], function(){
Route::get('profile', 'UserController@profile');
Route::post('profile', 'UserController@update_avatar');
Route::get('/tampil/post', 'PostController@tampil');
Route::get('/top/user', 'DetailController@index');
Route::get('/kategori/{kategori}','DetailController@kategori');
Route::get('/likes/{likes}','DetailController@likes');
Route::get('/decre/{likes}','DetailController@decres');
Route::resource('posts', 'PostsController');
Route::get('/about',function(){
	return view('about');
});



Route::get('/home1','DetailController@createDetail')->name('home');
Route::get('/home', 'HomeController@index')->name('home');

});

});

